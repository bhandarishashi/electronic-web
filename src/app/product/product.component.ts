import { CartService } from './../_services/cart.service';
import { CategoryService } from './../_services/category.service';
import { MetatagService } from './../_services/metatag.service';
import ProductService from './../_services/product.service';
import { environment } from 'src/environments/environment';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isPlatformBrowser } from "@angular/common";
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  product_key = '';
  product = {
    product_id: '',
    name: '',
    description: '',
    price: '',
    image: '',
    sku: '',
    tag: '',
    type: '',
    color: '',
    model_name: '',
    series: '',
    brand: '',
    stock: '',
    discount_price: '',
    images: [{
      thumbnail_image: '',
      full_image: ''
    }]
  }
  products = [{
    product_id: '',
    name: '',
    category_id: '',
    price: '',
    image: '',
    tag: '',
    discount_price: ''
  }];

  loader = true;
  page = 1;
  limit = 4;
  category_id = '';
  cart_id = '';
  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService, private categoryService: CategoryService, private metaTagService: MetatagService,
    @Inject(PLATFORM_ID) private platformId: Object, private cartService: CartService, private cookieService: CookieService, private router: Router) {
    this.activatedRoute.params.subscribe(param => {
      this.product_key = param.product_key;
      this.productService.getProductDetail(param.product_key).subscribe(res => {
        this.product = res.body.product;
        this.category_id = res.body.product.category_id;
        this.recommendedProduct(this.category_id);
        this.metaTagService.setTags(this.product.name + ' | Sassy Electronics');
        this.loader = false;
      })
    });
    if (isPlatformBrowser(this.platformId)) {
      if (this.cookieService.get('cart_id') != 'undefined') {
        this.cart_id = this.cookieService.get('cart_id');
      }
    }



  }

  ngOnInit(): void {

  }

  scrollToDescription() {
    if (isPlatformBrowser(this.platformId)) {
      var elmnt = document.getElementById("description")!;
      elmnt.scrollIntoView();
    }

  }

  recommendedProduct(category_id: any) {
    this.loader = true;
    this.categoryService.getRecommendedProducts(category_id, this.product_key, this.page, this.limit).subscribe(res => {
      this.products = res.body.products;
      this.loader = false;
    })
  }

  showImage(fullImage: string) {
    this.product.image = fullImage;
  }

  addToCart(price: any) {
    var quantity = 0, sub_total = 0, total = 0;
    var quantityTemp = parseFloat((<HTMLInputElement>document.getElementById("qty")).value);
    if (quantityTemp == 1 || quantityTemp <= 5) {
      quantity = quantityTemp;
    }
    sub_total = (price * quantity);
    total = sub_total;

    var data = {
      cart_id: this.cart_id,
      product_id: this.product_key,
      quantity: quantity,
      image: this.product.image,
      price: price,
      sub_total: sub_total,
      total: total
    };
    this.cartService.submitCart(data).subscribe(res => {
      if (res.status == 201) {
        var cartId = res.body.cart_id;
        this.cookieService.set('cart_id', cartId, { path: '/', domain: environment.cookieDomain });
        (<HTMLInputElement>document.getElementById("qty")).value = "1";
        this.router.navigate(['/cart'])
      }

    }, err => {

    })
  }
}
