import { MetatagService } from './../_services/metatag.service';
import ProductService from './../_services/product.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products = [{
    product_id: '',
    name: '',
    category_id: '',
    price: '',
    image: '',
    tag: '',
    discount_price: ''
  }];

  slider = [
    {
      image: 'assets/images/main-slider/main-img1.png'
    },
    {
      image: 'assets/images/main-slider/main-img2.png'
    },
    {
      image: 'assets/images/main-slider/main-img3.png'
    },
  ];
  constructor(private productService: ProductService, private router: Router, private metaTagService: MetatagService) {
    this.getProductList();
  }
  loader = true;
  ngOnInit(): void {
    this.metaTagService.setTags('Home | Sassy Electronics');

  }
  getProductList() {
    this.productService.getProducts().subscribe(res => {
      this.products = res.body.featured_products.rows;
      this.loader = false;
    }, err => {
      this.loader = false;
      this.products = [];
      // if (err.status == 404) {
      //   this.router.navigate(['/404']);
      // }
    });

  }

}
