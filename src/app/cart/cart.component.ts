import { ActivatedRoute } from '@angular/router';
import { CartService } from './../_services/cart.service';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartId = '';
  cartItems = [{
    cart_id: '',
    product_id: '',
    price: '',
    quantity: '',
    total: '',
    sub_total: '',
    product: {
      image: '',
      name: '',
      sku: ''
    }
  }];
  showErrorMsg = false;
  errorMessage = '';
  subTotal = 0;
  subQuanity = 0;
  constructor(private cartService: CartService, private activatedRoute: ActivatedRoute, private cookieService: CookieService) {
    if (this.cookieService.get('cart_id') != undefined) {
      this.cartId = this.cookieService.get('cart_id');
      this.cartDetail(this.cartId);
    }
  }
  loader = true;
  ngOnInit(): void {
  }

  cartDetail(cart_id: any) {
    this.cartService.getCartDetail(cart_id).subscribe(res => {
      this.cartItems = res.body.cart;
      this.subTotal = parseFloat(this.cartItems.map(item => item.total).reduce((acc, next) => acc + next));
      this.subQuanity = parseFloat(this.cartItems.map(item => item.quantity).reduce((acc, next) => acc + next));
      this.loader = false;
      if (this.cartItems.length == 0) {
        this.errorMessage = 'Your cart is empty.'
      }
    }, error => {
      this.loader = false;
      this.cartItems = [];
      if (error.status == 404) {
        this.showErrorMsg = true;
        this.errorMessage = 'Your cart is empty.'
      }
    })
  }

  removeItem(cartId: string, productId: string) {
    alert('Are you sure you want to remove this item?');
    this.cartService.removeCartItem(cartId, productId).subscribe(res => {
      this.cartDetail(cartId);
    }, error => {

    })
  }
}
