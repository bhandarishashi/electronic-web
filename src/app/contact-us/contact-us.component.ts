import { ContactService } from '../_services/contact.service';
import { MetatagService } from './../_services/metatag.service';
import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { isPlatformBrowser } from "@angular/common";

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactForm: any;
  contactCheckForm = false;
  successMessage = '';
  errorMessage = '';

  constructor(private fb: FormBuilder, @Inject(PLATFORM_ID) private platformId: Object, private contactService: ContactService,
    private metaTagService: MetatagService) { }

  ngOnInit(): void {
    this.metaTagService.setTags('Contact Us | Sassy Electronics');
    this.contactForm = this.fb.group({
      full_name: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.minLength(2)]),
      email: new FormControl('', [Validators.email]),
      subject: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required])
    });
  }

  submitcontactForm() {
    let isFormValid = true
    let errorCOntorlName = [];
    Object.keys(this.contactForm.controls).forEach((value) => {
      let currentControl = this.contactForm.controls[value];
      if (currentControl.invalid) {
        currentControl.markAsTouched();
        isFormValid = false;
        errorCOntorlName.push(value)
      } else if (currentControl.value.trim() == '') {
        this.contactForm.get(value).markAsTouched()
        this.contactForm.get(value).setErrors({ onlySpace: true });
      }

    });
    if (isFormValid) {
      this.contactCheckForm = true;
      this.contactService.submitMessage(this.contactForm.value).subscribe(res => {
        this.successMessage = res.body.message;
        this.contactForm.reset();
        this.contactCheckForm = false;
        if (isPlatformBrowser(this.platformId)) {
          window.scrollTo(0, 0);
        }

      }, err => {
        this.contactCheckForm = false;
        this.errorMessage = 'Message has  not been  submitted.';

      })
    } else {
      this.contactCheckForm = false;
      if (isPlatformBrowser(this.platformId)) {
        window.scrollTo(0, 0);
      }
    }

  }
}


