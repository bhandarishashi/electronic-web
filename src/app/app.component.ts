import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { isPlatformBrowser } from "@angular/common"
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SassyElectronics';
  constructor(
    private router: Router, private cookieService: CookieService, @Inject(PLATFORM_ID) private platformId: Object
  ) {
    if (isPlatformBrowser(this.platformId)) {
      this.router.events.subscribe((e) => {
        if (e instanceof NavigationEnd) {
          // Function you want to call here
          this.hideMenu();
        }
      });
    }
  }


  hideMenu() {
    if (isPlatformBrowser(this.platformId)) {
      var menu = document.getElementById('navbarCollapse');
      menu!.classList.remove('in');
      menu!.classList.add('collapse');
    }
  }
}
