import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  apiUrl = environment.apiUrl;
  frontendUrl = environment.frontendUrl;
  categoryUrl = '';
  constructor(private http: HttpClient) {
    this.categoryUrl = this.apiUrl + 'category/';
  }

  //get list of categories
  getCategories(): Observable<any> {
    return this.http.get<any>(this.categoryUrl, {
      observe: 'response'
    });
  }

  //get product by category
  getCategoryProducts(key: string, page: number, limit: number): Observable<any> {
    return this.http.get<any>(this.categoryUrl + key + '?page=' + page + '&limit=' + limit, {
      observe: 'response'
    });
  }


  //get product by category
  getRecommendedProducts(category_id: string, product_id: string, page: number, limit: number): Observable<any> {
    return this.http.get<any>(this.categoryUrl + 'recommend/' + category_id + '?product_id=' + product_id + '&page=' + page + '&limit=' + limit, {
      observe: 'response'
    });
  }


}
